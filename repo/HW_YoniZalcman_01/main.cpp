#include <iostream>
#include "queue.h"
void main()
{
	myQue* s = new myQue;
	s->que = initqueue(&s->position, &s->max, &s->last, 3);
	enqueue(&s->que, &s->last, s->max, 3);
	enqueue(&s->que, &s->last, s->max, 6);
	enqueue(&s->que, &s->last, s->max, 2);
	std::cout << "First program in C++" << std::endl;
	while (!isEmpty(s->position, s->last))
	{
		std::cout << "Element was dropped:" << dequeue(&s->que, &s->position, &s->last) << std::endl;
	}
	cleanqueue(&s->que);
	system("pause");
}
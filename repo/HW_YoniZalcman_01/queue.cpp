#include <stdio.h>
#include "queue.h"
void enqueue(int** que, int* last, int max, int num)
{
	if (!isFull(*last,max))
	{
		(*que)[*last] = num;
		(*last)++;
	}
}

int dequeue(int** que, int* position, int* last)
{
	int ans = -1;
	if (!isEmpty(*position,*last))
	{
		ans = (*que)[*position];
		(*position)++;
	}
	return ans;
}

int* initqueue(int* position,int* max, int* last,int num)
{
	int* que = new int[num];
	*position = 0;
	*last = 0;
	*max = num;
	return que;
}

void cleanqueue(int** que)
{
	delete *(que);
}

bool isFull(int position, int max)
{
	return(max == position);
}

bool isEmpty(int position, int last)
{
	return(position == last);
}
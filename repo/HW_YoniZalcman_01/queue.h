#pragma once
struct myStruct
{
	int position;
	int last;
	int max;
	int* que;
};
typedef struct myStruct myQue;
void enqueue(int** que, int* last, int max, int num);
int dequeue(int** que, int* position, int* last);
int* initqueue(int* position, int* max, int* last, int num);
void cleanqueue(int** que);
bool isFull(int position, int max);
bool isEmpty(int position, int last);

#include "linkedList.h"
#include <iostream>

myStack* creatNewLink()
{
	myStack* link = new myStack;
	link->next = NULL;
	link->num = 0;
	return link;
}


myStack* push(myStack** stack, int num)
{
	myStack* link = creatNewLink();
	if (!(*stack))
	{
		link->next = NULL;
		link->num = num;
	}
	else
	{
		link->next = (*stack);
		link->num = num;
	}
	return link;
}

int pop(myStack** stack)
{
	int ans = 0;
	myStack* temp = 0;
	temp = *stack;
	ans = temp->num;
	*stack=(*stack)->next;
	delete temp;
	return ans;
}


void cleanStack(myStack** stack)
{
	myStack* temp = NULL;
	while (*stack)
	{
		temp = *stack;
		*stack = (*stack)->next;
		delete(temp);
	}
	*stack = NULL;
}
struct myLinkedStack
{
	struct myLinkedStack* next;
	int num;
};
typedef struct myLinkedStack myStack;
myStack* creatNewLink();
int pop(myStack** stack);
myStack* push(myStack** stack, int num);
void cleanStack(myStack** stack);
#include "stack.h"
#include <iostream>

void push(stack *s, int element)
{
	
	add(s->_elements, element);
	s->_count++;
}

int pop(stack *s)
{
	if(!isEmpty(s))
	{
		s->_count--;
		return removeHead(s->_elements);
	}
	else
	{
		return 0;
	}
}

void initStack(stack *s)
{
	s->_count = 0;
	s->_elements = new myLinkedStack;
	initList(s->_elements);
}


void cleanStack(stack *s)
{
	cleanList(s->_elements);
	delete s->_elements;
	s->_count = 0;
}

bool isEmpty(stack* s)
{
	return s->_count == 0;
}

bool isFull(stack* s)
{
	// can never be full. because it is base on linked list
	return false;
}
